import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Objet Machine
 * @author nicolas
 */
public class Machine {

    /**
     * Boissons présentes dans la machine
     */
    private HashMap<String, Boisson> boissons;

    /**
     * Ingrédients présents dans la machine
     */
    private HashMap<String, Ingredients> ingredients;

    /**
     * Singleton
     */
    private static Machine instance;

    /**
     * Instance de fichier utilisé pour récupérer les informations sur les boissons et ingredients
     */
    private Fichier fichier;

    /**
     * Stock maximum d'un ingredient dans la machine
     */
    public static final int STOCK_MAXIMUM_INGREDIENTS = 10;

    /**
     * Nombre de maximum de boisson dans la machine
     */
    public static final int NOMBRE_MAXIMUM_DE_BOISSON = 5;



    /**
     * Constructeur de la machine
     */
    public Machine() {
        this.boissons = new HashMap<>();
        this.ingredients = new HashMap<>();

        this.fichier = new Fichier();

        this.remplirListeBoissons();
        this.remplirListeIngredients();
    }

    /**
     * Achat d'une boisson
     *
     * @param boisson boisson que l'on achete
     * @param solde   montant de l'argent inséré dans la machine
     * @return le message correspondant
     */
    public String acheterBoisson(Boisson boisson, int solde, int quantiteSucre) {
        String message = "";

        if(boisson.getUnitesCafe() <= this.ingredients.get("Cafe").getStock()
                && boisson.getUnitesLait() <= this.ingredients.get("Lait").getStock()
                && boisson.getUnitesChocolat() <= this.ingredients.get("Chocolat").getStock()
                && quantiteSucre <= this.ingredients.get("Sucre").getStock()) {

            // Vérification que le montant inséré est suffisant
            if (solde >= boisson.getPrix()) {
                // Si le montant insérer est supérieur au prix on rend la monnaie
                if (solde > boisson.getPrix()) {
                    int soldeARendre = solde - boisson.getPrix();
                    message = "Veuillez récupérer votre argent. Montant : " + soldeARendre + "€.";
                }

                this.ingredients.get("Cafe").setStock(this.ingredients.get("Cafe").getStock() - boisson.getUnitesCafe());
                this.ingredients.get("Lait").setStock(this.ingredients.get("Lait").getStock() - boisson.getUnitesLait());
                this.ingredients.get("Chocolat").setStock(this.ingredients.get("Chocolat").getStock() - boisson.getUnitesChocolat());
                this.ingredients.get("Sucre").setStock(this.ingredients.get("Sucre").getStock() - quantiteSucre);

                message += "\nVotre boisson est prête avec " + quantiteSucre + " sucre.";
            } else {
                message = "Le montant inséré dans la machine n'est pas suffisant.";
                message += "\nVeuillez récupérer votre argent. Montant : " + solde + "€";
            }
        } else {
            message = "Désolé, il n'y a plus assez d'ingrédients pour votre boisson.";
            message += "\nVeuillez récupérer votre argent. Montant : " + solde + "€";
        }

        return message;
    }

    /**
     * Ajout d'une boisson
     *
     * @param boisson boisson que l'on ajoute
     * @return le message correspondant
     */
    public String ajouterBoisson(Boisson boisson) {
        String message;

        // Vérification que le nombre de boisson n'est pas atteint
        if(this.boissons.size() >= NOMBRE_MAXIMUM_DE_BOISSON) {
            message = "Le nombre limite de boissons (3) est déjà atteint. Veuillez supprimer une boisson avant de l'ajouter.";
        }
        // Vérification que la boisson n'existe pas déjà
        else if (this.boissons.containsKey(boisson.getNom())) {
            message = "Une boisson possédant déjà ce nom existe déjà.";
        }
        // Ajout de la boisson
        else {
            this.boissons.put(boisson.getNom(), boisson);
            message = "La boisson a bien été ajoutée.";
        }

        return message;
    }

    /**
     * Modifie une boisson
     *
     * @param boisson         boisson à modifier
     * @param nouvellesInformations nouvelles valeurs de la boisson
     */
    public void modifierBoisson(Boisson boisson, ArrayList<Integer> nouvellesInformations) {
        // Enregistrement des nouvelles données de la boisson
        boisson.setPrix(nouvellesInformations.get(0)); // prix
        boisson.setUnitesCafe(nouvellesInformations.get(1)); // cafe
        boisson.setUnitesChocolat(nouvellesInformations.get(2)); // chocolat
        boisson.setUnitesLait(nouvellesInformations.get(3)); // lait

        int doseSucre = nouvellesInformations.get(4);
        if(doseSucre > Sucre.QUANTITE_MAX) {
            doseSucre = Sucre.QUANTITE_MAX;
        }
        boisson.setUnitesSucre(doseSucre); // sucre
    }

    /**
     * Suppression d'une boisson
     *
     * @param boisson boisson à supprimer
     */
    public void supprimerBoisson(Boisson boisson) {
        this.boissons.remove(boisson.getNom());
    }

    /**
     * Rechargement de tous les ingrédients à 10 de stock.
     * Si le stock n'est pas vide on complète afin d'atteindre 10.
     */
    public void rechargerIngredients() {
        // Pour chaque ingrédients on complète jusque 10
        for(Map.Entry<String, Ingredients> ingredient : this.ingredients.entrySet()) {
            ingredient.getValue().remplirStock();
        }
    }

    /**
     * Affiche le stock des ingrédients
     *
     * @return le message avec les stocks correspondants
     */
    public String verifierStocksIngredients() {
        String message = "";

        for(Map.Entry<String, Ingredients> ingredient : this.ingredients.entrySet()) {
            message += "Il reste " + ingredient.getValue().getStock() + " unités de " + ingredient.getKey() + ".\n";
        }

        return message;
    }

    /**
     * Informe si la machine est pleine ou non
     * @return si la machine est vide
     */
    public boolean machineVide() {
        return this.getBoissons().isEmpty();
    }

    /**
     * Singleton de la machine
     *
     * @return l'instance courante
     */
    public static Machine getInstance() {
        // on créé seulement l'instance la première fois
        if (instance == null) {
            instance = new Machine();
        }

        return instance;
    }

    /**
     * Met à jour l'attribut boisson en fonction des informations contenu dans le fichier
     */
    public void remplirListeBoissons() {
        ArrayList<Boisson> boissons = this.fichier.getBoissonsFromFile();

        for (Boisson boisson : boissons) {
            this.boissons.put(boisson.getNom(), boisson);
        }
    }

    /**
     * Met à jour l'attribut ingredient en fonction des informations contenu dans le fichier
     */
    public void remplirListeIngredients() {
        ArrayList<Ingredients> ingredients = this.fichier.getIngredientsFromFile();

        for (Ingredients ingredient : ingredients) {
            this.ingredients.put(ingredient.getNom(), ingredient);
        }
    }

    /**
     * Récupère les boissons et ingredients afin de les transmettre à la gestion des fichiers
     */
    public void sauvegarderChangements() {
        ArrayList<Ingredients> ingredientsTmp = new ArrayList<>();
        ArrayList<Boisson> boissonsTmp = new ArrayList<>();

        // On récupère les objets Boisson
        for(Map.Entry<String, Boisson> boisson : this.boissons.entrySet()) {
            boissonsTmp.add(boisson.getValue());
        }

        // On récupère les objets Ingredients
        for(Map.Entry<String, Ingredients> ingredient : this.ingredients.entrySet()) {
            ingredientsTmp.add(ingredient.getValue());
        }

        // On sauvegarde dans le fichier
        fichier.sauvegarderBoissons(boissonsTmp);
        fichier.sauvegarderIngredient(ingredientsTmp);
    }

    /**
     * Accesseur de boissons
     *
     * @return les boissons
     */
    public HashMap getBoissons() {
        return this.boissons;
    }

}