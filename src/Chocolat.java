/**
 * Objet Chocolat hérité de Ingredients
 * @author nicolas
 */
public class Chocolat extends Ingredients {

    /**
     * Constructeur
     * @param stock stock restant
     */
    public Chocolat(int stock) {
        super();
        this.nom = "Chocolat";
        this.stock = stock;
    }

}
