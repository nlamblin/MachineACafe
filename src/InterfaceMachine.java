import javax.lang.model.element.QualifiedNameable;
import java.util.*;

/**
 * Simule l'interface graphique
 * @author nicolas
 */
public class InterfaceMachine {

    /**
     * Machine à café utilisée
     */
    private Machine machine;



    /**
     * Constructeur de l'interface machine
     */
    public InterfaceMachine() {
        this.machine = Machine.getInstance();
    }

    /**
     * Affichage du menu général de la machine à café
     */
    public void debut() {
        System.out.println("Bonjour !\n");
        boolean stop = false;

        // Tant qu'on arrete pas le programme
        while(!stop) {
            Boisson boisson;
            String message = "";

            System.out.println("Quelle action souhaitez-vous effectuer ? \n");

            System.out.println("1 - Acheter une boisson");
            System.out.println("2 - Ajouter une boisson");
            System.out.println("3 - Modifier une boisson");
            System.out.println("4 - Supprimer une boisson");
            System.out.println("5 - Afficher le stock des ingrédients");
            System.out.println("6 - Recharger les ingrédients");
            System.out.println("7 - Quitter");

            System.out.println("\nVeuillez saisir l'identifiant de l'action à effectuer : ");
            int identifiantAction = this.verificationEntier(true);

            // choix de l'action
            switch (identifiantAction) {
                case 1:
                    if(machine.machineVide()) {
                        message = "Aucune boisson n'est présente dans la machine.";
                    }
                    else {
                        System.out.println("Quelle boisson souhaitez-vous acheter ?\n");
                        boisson = this.choixBoisson();

                        System.out.println("\nVoulez-vous ajouter ou retirer du sucre (o pour oui / autre caractère pour non) ?");
                        Scanner scanner = new Scanner(System.in);
                        String choixSucre = scanner.nextLine();
                        int quantiteSucre = boisson.getUnitesSucre();
                        if(choixSucre.equals("o")) {
                            quantiteSucre = this.modifierQuantiteSucre(quantiteSucre);
                        }

                        System.out.println("Veuillez insérer l'argent dans la machine : ");
                        int solde = verificationEntier(false);

                        message = machine.acheterBoisson(boisson, solde, quantiteSucre);
                    }
                    break;
                case 2:
                    boisson = this.menuAjouterBoisson();
                    if(boisson != null) message = machine.ajouterBoisson(boisson);
                    break;
                case 3:
                    if(machine.machineVide()) {
                        message = "Aucune boisson n'est présente dans la machine.";
                    }
                    else {
                        boisson = this.choixBoisson();
                        ArrayList<Integer> nouvellesInformations = this.menuModifierBoisson();

                        if (!nouvellesInformations.isEmpty()) {
                            machine.modifierBoisson(boisson, nouvellesInformations);
                            System.out.println("La boisson a bien été modifiée.");
                        }
                    }
                    break;
                case 4:
                    if(machine.machineVide()) {
                        message = "Aucune boisson n'est présente dans la machine.";
                    }
                    else {
                        System.out.println("Quelles boissons souhaitez-vous supprimer ?\n");
                        boisson = this.choixBoisson();
                        machine.supprimerBoisson(boisson);
                        message = "La boisson a bien été supprimée.";
                    }
                    break;
                case 5:
                    message = machine.verifierStocksIngredients();
                    break;
                case 6:
                    machine.rechargerIngredients();
                    message = "Les ingrédients ont bien été rechargés.";
                    break;
                case 7:
                    machine.sauvegarderChangements();
                    stop = true;
                    message = "Merci et à bientôt !";
                    break;
                default:
                    message = "Cette action n'existe pas.";
            }

            System.out.println("\n" + message + "\n");
        }
    }

    /**
     * Menu du choix de la boisson
     * @return la boisson selectionnée
     */
    private Boisson choixBoisson() {
        // création d'un itérateur pour parcourir la HashMap
        Iterator iterator = machine.getBoissons().entrySet().iterator();
        HashMap<Integer, Boisson> listeBoissons = new HashMap<>();
        // utilisation d'un index pour savoir quelle boisson sera choisie
        int index = 0;

        // pour chaque élement de la HashMap
        while (iterator.hasNext()) {
            Map.Entry boisson = (Map.Entry) iterator.next(); // récupération de la boisson
            listeBoissons.put(index, (Boisson) boisson.getValue()); // ajout à la liste la boisson
            index++;

            System.out.println(index + " : " + boisson.getValue().toString());
        }

        // Saisi de l'identifiant
        System.out.println("\nVeuillez saisir l'identifiant de la boisson concernée :");
        int identifiantBoisson = this.verificationEntier(true);

        if(identifiantBoisson-1 < listeBoissons.size()) {
            return listeBoissons.get(identifiantBoisson-1); // retourne la boisson choisie
        }
        else {
            System.out.println("\nL'identifiant saisi n'est pas valide.");
            return this.choixBoisson();
        }
    }

    /**
     * Modifie la quantité de sucre souhaitée
     * @param quantiteSucre quantité de sucre actuelle de la boisson
     * @return la nouvelle quantité de sucre souhaitée
     */
    private int modifierQuantiteSucre(int quantiteSucre) {
        System.out.println("\nVeuillez saisir + ou - afin d'ajouter ou retirer du sucre.");

        Scanner scanner = new Scanner(System.in);
        String choix = scanner.nextLine();

        String verifChoix = choix.replace("+", "-").replace("-", "");

        // On vérifie que tous les caractères de la chaine sont seulement + ou -
        if(verifChoix.length() != 0) {
            System.out.println("Votre saisi n'est pas valide. Vous devez saisir que + ou -.");
            return this.modifierQuantiteSucre(quantiteSucre);
        }

        String[] chaineSplit = choix.split("");
        int nouvelleQuantiteSucre = quantiteSucre;

        for (String charSplit : chaineSplit) {
            if (charSplit.equals("+")) {
                if(quantiteSucre < Sucre.QUANTITE_MAX) {
                    nouvelleQuantiteSucre++;
                }
            }
            else {
                if (nouvelleQuantiteSucre > 0) {
                    nouvelleQuantiteSucre--;
                }
            }
        }

        return nouvelleQuantiteSucre;
    }

    /**
     * Menu d'ajout de la boisson
     * @return la boisson à ajouter
     */
    private Boisson menuAjouterBoisson() {
        // Saisie des informations de la boisson
        Scanner scanner = new Scanner(System.in);

        boolean nomValide = false;
        String nomBoisson = null;
        // tant que le nom n'est pas valide
        while(!nomValide) {
            System.out.println("Quel est le nom de votre boisson : ");
            nomBoisson = scanner.nextLine();

            if(!nomBoisson.equals("")) {
                nomBoisson.replaceAll("\n", "");
                nomValide = true;
            }
            else {
                System.out.println("Nom incorrect");
            }
        }

        System.out.println("Quel est le prix de votre boisson : ");
        int prixBoisson = this.verificationEntier(true);

        System.out.println("Quelle est la dose de café requise ? : ");
        int doseCafe = this.verificationEntier(false);

        System.out.println("Quelle est la dose de chocolat requise ? : ");
        int doseChocolat = this.verificationEntier(false);

        System.out.println("Quelle est la dose de sucre requise ? : ");
        int doseSucre = this.verificationEntier(false);

        System.out.println("Quelle est la dose de lait requise ? : ");
        int doseLait = this.verificationEntier(false);

        if(!this.verifierConformite(doseCafe, doseChocolat, doseSucre, doseLait)) {
            System.out.println("La boisson n'est pas valide. \n");
            return null;
        }
        else {
            return new Boisson(nomBoisson, prixBoisson, doseCafe, doseChocolat, doseSucre, doseLait); // retourne la boisson créée
        }
    }

    /**
     * Menu de modification de la boisson
     * @return un tableau avec les nouvelles valeurs des ingrédients et le prix
     */
    private ArrayList<Integer> menuModifierBoisson() {
        // Saisie des nouvelles informations de la boisson
        System.out.println("Quel est le nouveau prix de la boisson ? : ");
        int prixBoisson = this.verificationEntier(true);

        System.out.println("Quelle est la nouvelle dose de café requise ? : ");
        int doseCafe = this.verificationEntier(false);

        System.out.println("Quelle est la nouvelle dose de chocolat requise ? : ");
        int doseChocolat = this.verificationEntier(false);

        System.out.println("Quelle est la nouvelle dose de sucre requise ? : ");
        int doseSucre = this.verificationEntier(false);

        System.out.println("Quelle est la nouvelle dose de lait requise ? : ");
        int doseLait = this.verificationEntier(false);

        ArrayList<Integer> boissonModifiee = new ArrayList<>();

        if(this.verifierConformite(doseCafe, doseChocolat, doseLait, doseSucre)) {
            boissonModifiee.add(prixBoisson);
            boissonModifiee.add(doseCafe);
            boissonModifiee.add(doseChocolat);
            boissonModifiee.add(doseLait);
            boissonModifiee.add(doseSucre);
        }
        else {
            System.out.println("La boisson n'est pas valide");
        }

        return boissonModifiee;
    }

    /**
     * Verifie si la valeur saisie est un entier
     * @param superieurZero indique si il s'agit d'un prix
     * @return la valeur
     */
    private int verificationEntier(boolean superieurZero) {
        boolean juste = false;
        int valeur = 0;

        // tant que la valeur saisie n'est pas un entier valide
        while(!juste) {
            boolean erreur = false;

            try {
                Scanner scanner = new Scanner(System.in);
                valeur = scanner.nextInt();
                if(superieurZero) {
                    // un prix doit etre supérieur à 0
                    if(valeur > 0) juste = true;
                    else erreur = true;
                }
                else {
                    // si il est positif ou nul
                    if(valeur >= 0) juste = true;
                    else erreur = true;
                }
            }
            catch (InputMismatchException e) {
                erreur = true;
            }

            if(erreur) {
                System.out.println("Votre saisie n'est pas valide. Veuillez saisir un entier.");
            }
        }

        return valeur;
    }

    /**
     * Permet de vérifier qu'une boisson est conforme
     * @param doseCafe dose de cafe
     * @param doseChocolat dose de chocolat
     * @param doseSucre dose de surcre
     * @param doseLait dose de lait
     * @return si la boisson est conforme ou non
     */
    private boolean verifierConformite(int doseCafe, int doseChocolat, int doseSucre, int doseLait) {
        boolean resultat = false;

        if(doseCafe > 0 || doseChocolat > 0 || doseSucre > 0 || doseLait > 0) {
            resultat = true;
        }

        return resultat;
    }
}