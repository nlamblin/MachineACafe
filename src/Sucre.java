/**
 * Objet Sucre hérité de Ingredients
 * @author nicolas
 */
public class Sucre extends Ingredients {

    /**
     * Quantité maximale de sucre autorisée
     */
    public final static int QUANTITE_MAX = 6;

    /**
     * Constructeur
     * @param stock stock restant
     */
    public Sucre(int stock) {
        super();
        this.nom = "Sucre";
        this.stock = stock;
    }
}