/**
 * Objet Boisson
 * @author nicolas
 */
public class Boisson {

    /**
     * Nom de la boisson
     */
    private String nom;

    /**
     * Prix de la boisson
     */
    private int prix;

    /**
     * Unités de café nécessaires à la confection de la boisson
     */
    private int unitesCafe;

    /**
     * Unités de lait nécessaires à la confection de la boisson
     */
    private int unitesLait;

    /**
     * Unités de sucre nécessaires à la confection de la boisson
     */
    private int unitesSucre;

    /**
     * Unités de chocolat nécessaires à la confection de la boisson
     */
    private int unitesChocolat;



    /**
     * Constrcuteur de boisson
     * @param nom nom de la boisson
     * @param prix prix de la boisson
     * @param unitesCafe unités de café nécessaires
     * @param unitesLait unités de lait nécessaires
     * @param unitesSucre unités de sucre nécessaires
     * @param unitesChocolat unités de chocolat nécessaire
     */
    public Boisson(String nom, int prix, int unitesCafe, int unitesLait, int unitesSucre, int unitesChocolat) {
        this.nom = nom;
        this.prix = prix;
        this.unitesCafe = unitesCafe;
        this.unitesLait = unitesLait;

        if(unitesSucre > Sucre.QUANTITE_MAX) {
            unitesSucre = Sucre.QUANTITE_MAX;
        }

        this.unitesSucre = unitesSucre;
        this.unitesChocolat = unitesChocolat;
    }

    /**
     * Affiche les informations de la boisson
     * @return le message d'information sur la boisson
     */
    public String toString() {
        return this.nom + " / " + this.prix + "€ ";
    }



    /**
     * Accesseur de nom
     * @return le nom de la boisson
     */
    public String getNom() {
        return nom;
    }

    /**
     * Accesseur de prix
     * @return le prix de la boisson
     */
    public int getPrix() {
        return this.prix;
    }

    /**
     * Mutateur de prix
     * @param prix nouveau prix de la boisson
     */
    public void setPrix(int prix) {this.prix = prix; }

    /**
     * Accesseur de unitesCafe
     * @return unitesCafe
     */
    public int getUnitesCafe() {
        return this.unitesCafe;
    }

    /**
     * Mutateur de unitesCafe
     * @param unitesCafe nouvelle unités de café nécessaires à la confection de la boisson
     */
    public void setUnitesCafe(int unitesCafe) {
        this.unitesCafe = unitesCafe;
    }

    /**
     * Accesseur de unitesLait
     * @return unitesLait
     */
    public int getUnitesLait() {
        return this.unitesLait;
    }

    /**
     * Mutateur de unitesLait
     * @param unitesLait nouvelle unités de lait nécessaires à la confection de la boisson
     */
    public void setUnitesLait(int unitesLait) {
        this.unitesLait = unitesLait;
    }

    /**
     * Accesseur de unitesSucre
     * @return unitesSucre
     */
    public int getUnitesSucre() {
        return this.unitesSucre;
    }

    /**
     * Mutateur de unitesSucre
     * @param unitesSucre nouvelle unités de sucre nécessaires à la confection de la boisson
     */
    public void setUnitesSucre(int unitesSucre) {
        this.unitesSucre = unitesSucre;
    }

    /**
     * Accesseur de unitesChocolat
     * @return unitesChocolat
     */
    public int getUnitesChocolat() {
        return this.unitesChocolat;
    }

    /**
     * Mutateur de unitesChocolat
     * @param unitesChocolat nouvelle unités de chocolat nécessaires à la confection de la boisson
     */
    public void setUnitesChocolat(int unitesChocolat) {
        this.unitesChocolat = unitesChocolat;
    }

}
