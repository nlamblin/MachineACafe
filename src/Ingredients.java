/**
 * Objet abstrait Ingredients
 * @author nicolas
 */
public abstract class Ingredients {

    /**
     * Nom de l'ingrédient
     */
    protected String nom;

    /**
     * Stock restant de l'ingrédient
     */
    protected int stock;




    /**
     * Recharge le stock à 10 unités
     */
    public void remplirStock() {
        // on calcule combien il manque pour arriver à 10 de stock
        int aRecharger = Machine.STOCK_MAXIMUM_INGREDIENTS - this.stock;
        // on ajoute le montant manquant au stock actuel
        this.stock = this.stock + aRecharger;
    }



    /**
     * Accesseur de stock
     * @return le stock restant pour l'ingrédient courant
     */
    public int getStock() {
        return this.stock;
    }

    /**
     * Mutateur de stock
     * @param stock nouveau stock de l'ingrédient courant
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * Accesseur de nom
     * @return le nom de l'ingrédient courant
     */
    public String getNom() {
        return this.nom;
    }
}
