/**
 * Objet Cafe hérité de Ingredients
 * @author nicolas
 */
public class Cafe extends Ingredients {

    /**
     * Constructeur
     * @param stock stock restant
     */
    public Cafe(int stock) {
        super();
        this.nom = "Cafe";
        this.stock = stock;
    }
}
