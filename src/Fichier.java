import java.io.*;
import java.util.ArrayList;


/**
 * Permet la gestion des fichiers
 * @author nicolas
 */
public class Fichier {

    /**
     * Répertoire des ressources
     */
    private static final String RESSOURCES = System.getProperty("user.dir") + "/resources/";

    /**
     * Fichier des ingrédients
     */
    private static final String FICHIER_INGREDIENTS = "ingredients.txt";

    /**
     * Fichier des boissons
     */
    private static final String FICHIER_BOISSONS = "boissons.txt";

    /**
     * Séprateur utilisé dans les fichiers
     */
    private static final String SEPARATOR = ";";

    /**
     * Charge un fichier
     * @param nomFichier nom du fichier à charger
     * @return FileReader du fichier
     */
    public FileReader chargerFichier(String nomFichier) {
        FileReader fr = null;
        try {
            File file = new File(RESSOURCES + nomFichier);
            fr = new FileReader(file);
        }
        catch (IOException e) {
            System.out.println("Erreur : Aucun fichier de sauvegarde n'a été trouvé.");
            System.exit(1);
        }

        return fr;
    }

    /**
     * Récupère les boissons contenues dans le fichiers
     * @return une liste de boissons
     */
    public ArrayList<Boisson> getBoissonsFromFile() {
        FileReader fr = chargerFichier(FICHIER_BOISSONS);
        ArrayList<Boisson> boissons = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(fr)) {
            String line;

            while((line = br.readLine()) != null) {
                String[] lineSplitted = line.split(SEPARATOR);

                String nom = lineSplitted[0];
                int prix = Integer.parseInt(lineSplitted[1]);
                int doseCafe = Integer.parseInt(lineSplitted[2]);
                int doseLait = Integer.parseInt(lineSplitted[3]);
                int doseChocolat = Integer.parseInt(lineSplitted[4]);
                int doseSucre = Integer.parseInt(lineSplitted[5]);

                Boisson boisson = new Boisson(nom, prix, doseCafe, doseLait, doseChocolat, doseSucre);
                boissons.add(boisson);
            }

            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return boissons;
    }

    /**
     * Récupère les ingrédients contenus dans le fichier
     * @return la liste des ingrédients
     */
    public ArrayList<Ingredients> getIngredientsFromFile() {
        FileReader fr = chargerFichier(FICHIER_INGREDIENTS);
        ArrayList<Ingredients> ingredients = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(fr)) {
            String line;

            while((line = br.readLine()) != null) {
                String[] lineSplitted = line.split(SEPARATOR);

                String nom = lineSplitted[0];
                int stock = Integer.parseInt(lineSplitted[1]);

                Ingredients ingredient = null;
                switch (nom) {
                    case "Cafe":
                        ingredient = new Cafe(stock);
                        break;
                    case "Lait":
                        ingredient = new Lait(stock);
                        break;
                    case "Chocolat":
                        ingredient = new Chocolat(stock);
                        break;
                    case "Sucre":
                        ingredient = new Sucre(stock);
                        break;
                }

                ingredients.add(ingredient);
            }

            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return ingredients;
    }

    /**
     * Ecrit le nouveau contenu dans un fichier
     * @param fichier fichier à modifier
     * @param nouveauContenu contenu à écrire dans le fichier
     */
    public void reecrireNouveauContenu(String fichier, String nouveauContenu) {
        try (FileWriter writer = new FileWriter(RESSOURCES + fichier)) {
            writer.write(nouveauContenu);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sauvegarde les boissons dans le fichier
     * @param boissons la liste des boissons à sauvegarder
     */
    public void sauvegarderBoissons(ArrayList<Boisson> boissons) {
        String nouveauContenu = "";
        for(Boisson boisson : boissons) {
            nouveauContenu += boisson.getNom() + SEPARATOR
                    + boisson.getPrix() + SEPARATOR
                    + boisson.getUnitesCafe() + SEPARATOR
                    + boisson.getUnitesLait() + SEPARATOR + boisson.getUnitesChocolat()
                    + SEPARATOR + boisson.getUnitesSucre()
                    + "\n";
        }

        this.reecrireNouveauContenu(FICHIER_BOISSONS, nouveauContenu);
    }

    /**
     * Sauvegarde les ingrédients dans le fichier
     * @param ingredients la liste des ingrédients à sauvegarder
     */
    public void sauvegarderIngredient(ArrayList<Ingredients> ingredients) {
        String nouveauContenu = "";
        for(Ingredients ingredient : ingredients) {
            nouveauContenu += ingredient.getNom() + SEPARATOR
                    + ingredient.getStock()
                    + "\n";
        }

        this.reecrireNouveauContenu(FICHIER_INGREDIENTS, nouveauContenu);
    }

}