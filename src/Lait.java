/**
 * Objet Lait hérité de Ingredients
 * @author nicolas
 */
public class Lait extends Ingredients {

    /**
     * Constructeur
     * @param stock stock restant
     */
    public Lait(int stock) {
        super();
        this.nom = "Lait";
        this.stock = stock;
    }
}
