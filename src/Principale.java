/**
 * Point d'entré de l'application
 * @author nicolas
 */
public class Principale {

    /**
     * Méthode d'entrée du programme
     * @param args arguments du programme
     */
    public static void main(String[] args) {
        Machine.getInstance(); // Création de la machine avec un singleton

        InterfaceMachine interfaceMachine = new InterfaceMachine(); // Création de l'interface
        interfaceMachine.debut(); // Lancement de l'application
    }

}