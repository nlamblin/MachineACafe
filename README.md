# Machine à café

TP de génie logiciel.

**Objectif :** Simuler le comportement d'une machine à café.

## Utilisation

Pour lancer l'application lancer la classe `Principale.java`.

## Développements

### Sources

Les fichiers `.java` se trouve dans le répoertoire `/src`.

### Ressources

Les fichiers `.txt` utilisés pour stocker les informations sur les boissons et les ingrédients sont dans `/resources`.

### Tests

Aucun test effectué.

## Documentation

La documentation se trouve à cette <a href="javadoc/index.html">adresse</a>.

## Contributeurs

* <a href="www.github.com/nlamblin">Nicolas Lamblin</a>